package duck_example.behaviors.quack;

import duck_example.interfaces.QuackBehavior;

public class Quack implements QuackBehavior {
  public Quack() {

  }

  public void quack() {
    System.out.println("Quack");
  }
}