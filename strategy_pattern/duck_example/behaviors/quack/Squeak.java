package duck_example.behaviors.quack;

import duck_example.interfaces.QuackBehavior;

public class Squeak implements QuackBehavior {
  public Squeak(){

  }

  public void quack() {
    System.out.println("Squeak");
  }
}