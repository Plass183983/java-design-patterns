package duck_example.behaviors.quack;

import duck_example.interfaces.QuackBehavior;

public class MuteQuack implements QuackBehavior {
  public MuteQuack() {

  }

  public void quack() {
    System.out.println("...");
  }
}