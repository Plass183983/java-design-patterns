package duck_example.behaviors.fly;

import duck_example.interfaces.FlyBehavior;

public class FlyWithWings implements FlyBehavior {
  public FlyWithWings() {}

  public void fly() {
    System.out.println("Flying with wings!");
  }
}