package duck_example.behaviors.fly;

import duck_example.interfaces.FlyBehavior;

public class FlyRocketPowered implements FlyBehavior{
  public FlyRocketPowered() {}

  public void fly() {
    System.out.println("I am flying at rocket-speed.");
  }
}
