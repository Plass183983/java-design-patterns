package duck_example.behaviors.fly;

import duck_example.interfaces.FlyBehavior;

public class FlyNoWay implements FlyBehavior{
  public FlyNoWay() {}

  public void fly() {
    System.out.println("It cannot fly unless you throw it.");
  }
}