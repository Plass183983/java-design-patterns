package duck_example.ducks;

import duck_example.behaviors.fly.*;
import duck_example.behaviors.quack.*;

public class MallardDuck extends Duck {

  public MallardDuck() {
    this.quackBehavior = new Quack();
    this.flyBehavior = new FlyWithWings();
  }

  public void display() {
    System.out.println("I'm a real Duck and I can swim");
  }
}
