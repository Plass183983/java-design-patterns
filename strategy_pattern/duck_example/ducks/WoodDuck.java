package duck_example.ducks;

import duck_example.behaviors.fly.FlyNoWay;
import duck_example.behaviors.quack.MuteQuack;

public class WoodDuck extends Duck{
  public WoodDuck() {
    this.quackBehavior = new MuteQuack();
    this.flyBehavior = new FlyNoWay();
  }

  public void display() {
    System.out.println("I'm just a wooden duck!");
  }
}