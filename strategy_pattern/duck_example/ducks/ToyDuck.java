package duck_example.ducks;

import duck_example.behaviors.quack.Squeak;
import duck_example.behaviors.fly.FlyNoWay;

public class ToyDuck extends Duck{
  public ToyDuck() {
    this.quackBehavior = new Squeak();
    this.flyBehavior = new FlyNoWay();
  }

  public void display() {
    System.out.println("I'm just a ToyDuck");
  }
}