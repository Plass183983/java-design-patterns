package duck_example.ducks;

import duck_example.behaviors.fly.FlyNoWay;
import duck_example.behaviors.quack.MuteQuack;

public class ModelDuck extends Duck{
  public ModelDuck() {
    this.flyBehavior = new FlyNoWay();
    this.quackBehavior = new MuteQuack();
  }

  public void display() {
    System.out.println("Displaying the model duck.");
  }
}
