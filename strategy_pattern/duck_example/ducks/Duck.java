package duck_example.ducks;

import duck_example.interfaces.*;

public class Duck {
  protected FlyBehavior flyBehavior;
  protected QuackBehavior quackBehavior;

  Duck() {}

  public void swim() {
    System.out.println("All ducks can swim.");
  }

  public void performQuack() {
    this.quackBehavior.quack();
  }

  public void performFly() {
    this.flyBehavior.fly();
  }

  public void setFlyBehavior(FlyBehavior f) {
    this.flyBehavior = f;
  }

  public void setQuackBehavior(QuackBehavior q) {
    this.quackBehavior = q;
  }

}