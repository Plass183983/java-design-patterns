package duck_example.interfaces;

public interface FlyBehavior {
  public void fly();
}