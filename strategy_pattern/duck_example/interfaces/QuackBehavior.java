package duck_example.interfaces;

public interface QuackBehavior {
  public void quack();
}