package duck_example;

import duck_example.behaviors.fly.FlyRocketPowered;
import duck_example.ducks.*;

public class MiniDuckSimulator {
  public static void main(String[] args) {
    // Normal Duck
    Duck mallard = new MallardDuck();
    mallard.performFly();
    mallard.performQuack();

    // Toy Duck
    Duck rubberDuck = new ToyDuck();
    rubberDuck.performFly();
    rubberDuck.performQuack();

    // Wooden Duck
    Duck woodDuck = new WoodDuck();
    woodDuck.performFly();
    woodDuck.performQuack();

    Duck modelDuck = new ModelDuck();
    modelDuck.performFly();
    modelDuck.performQuack();
    modelDuck.setFlyBehavior( new FlyRocketPowered());
    modelDuck.performFly();

  }
}