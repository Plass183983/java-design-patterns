package weather_station;

import weather_station.interfaces.Subject;

import java.util.ArrayList;

import weather_station.interfaces.Observer;

public class WeatherData implements Subject {

  private ArrayList<Observer> observers;
  private float humidity;
  private float temperature;
  private float pressure;

  public WeatherData() {
    this.observers = new ArrayList<Observer>();
  }

  public void registerObserver(Observer o) {
    this.observers.add(o);
  }

  public void removeObserver(Observer o) {
    if(this.observers.contains(o)) {
      this.observers.remove(o);
    }
  }

  public void setMeasurements(float humidity, float temperature, float pressure) {
    this.humidity = humidity;
    this.temperature = temperature;
    this.pressure = pressure;
    notifyObservers();
  }

  public void notifyObservers() {
    for (Observer observer : this.observers) {
      observer.update(this.humidity, this.temperature, this.pressure);
    }
  }

  public float getHumidity() {
    return this.humidity;
  }

  public float getTemperature() {
    return this.temperature;
  }

  public float getPressure() {
    return this.pressure;
  }

  /*
  public void setHumidity(float h) {
    this.humidity = h;
  }

  public void setTemperature(float t) {
    this.temperature = t;
  }

  public void setPressure(float p) {
    this.pressure = p;
  }*/
}
