package weather_station.observers;

import weather_station.interfaces.Display;
import weather_station.interfaces.Observer;
import weather_station.interfaces.Subject;

public class StatisticsDisplay implements Observer, Display{
  private float humiditySum;
  private float temperatureSum;
  private float pressureSum;
  
  // average values
  private float avHumidity;
  private float avTemperature;
  private float avPressure;

  private int updateCounter = 0;

  // min values
  private float minHumidity = 1000f;
  private float minTemperature = 1000f;
  private float minPressure = 1000f;

  // max values
  private float maxHumidity = -1000f;
  private float maxTemperature = -1000f;
  private float maxPressure = -1000f;

  private Subject weatherData;

  public StatisticsDisplay(Subject weatherData) {
    this.weatherData = weatherData;
    this.weatherData.registerObserver(this);
  }

  private void calcAverageValues(float humidity, float temperature, float pressure) {
    this.humiditySum += humidity;
    this.temperatureSum += temperature;
    this.pressureSum += pressure;

    this.avHumidity = this.humiditySum / this.updateCounter;
    this.avTemperature = this.temperatureSum / this.updateCounter;
    this.avPressure = this.pressureSum / this.updateCounter;
  }

  
  private void updateMinMaxValues(float humidity, float temperature, float pressure) {
    if(this.minHumidity > humidity) {
      this.minHumidity = humidity;
    }

    if(this.maxHumidity < humidity) {
      this.maxHumidity = humidity;
    }

    if(this.minTemperature > temperature) {
      this.minTemperature = temperature;
    }

    if(this.maxTemperature < temperature ) {
      this.maxTemperature = temperature;
    }

    if(this.minPressure > pressure) {
      this.minPressure = pressure;
    }

    if(this.maxPressure < pressure) {
      this.maxPressure = pressure;
    }
  }

  public void update(float humidity, float temperature, float pressure) {
    this.updateCounter++;

    this.calcAverageValues(humidity, temperature, pressure);
    this.updateMinMaxValues(humidity, temperature, pressure);

    this.display();
  }

  public void display() {
    System.out.println("---------Statistics data: -----------------\n"+
    "(Data from "+ this.updateCounter + " measurements. ) \n " +
    "Average Humidity:  \t" + this.avHumidity + "\n" +
    "Min. Humidity:     \t" + this.minHumidity + "\n" +
    "Max. Humidity:     \t" + this.maxHumidity + "\n" +
    "------------------------------------------------ \n" +
    "Average Temperature:  \t" + this.avTemperature + "\n" +
    "Min. Temperature:     \t" + this.minTemperature + "\n" +
    "Max. Temperature:     \t" + this.maxTemperature + "\n" +
    "------------------------------------------------\n" +
    "Average Pressure: " + this.avPressure + "\n" +
    "Min. Pressure: " + this.minPressure + "\n" +
    "Max. Pressure: " + this.maxPressure + "\n"
    );
  }

}
