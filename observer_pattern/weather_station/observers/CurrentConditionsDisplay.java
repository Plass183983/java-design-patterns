package weather_station.observers;

import weather_station.interfaces.Display;
import weather_station.interfaces.Observer;
import weather_station.interfaces.Subject;

public class CurrentConditionsDisplay implements Observer, Display {
  private float humidity;
  private float temperature;
  private Subject weatherData;

  public CurrentConditionsDisplay(Subject weatherData) {
    this.weatherData = weatherData;
    this.weatherData.registerObserver(this);
  }

  public void display() {
    System.out.println(
      "Current conditions are: \n" + 
      "humidity: " + this.humidity + "\n" + 
      "temperature:" + this.temperature + "\n"
      );
  }

  public void update(float humidity, float temperature, float pressure) {
    this.temperature = temperature;
    this.humidity = humidity;
    this.display();
  }
}
