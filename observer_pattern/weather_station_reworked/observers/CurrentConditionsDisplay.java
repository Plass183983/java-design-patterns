package weather_station_reworked.observers;

import java.util.Observer;
import java.util.Observable;
import weather_station_reworked.WeatherData;

public class CurrentConditionsDisplay implements Observer, Display {
  private float humidity;
  private float temperature;
  private Subject weatherData;

  public CurrentConditionsDisplay(Observerable weatherData) {
    this.weatherData = weatherData;
    this.weatherData.addObserver(this);
  }

  public void display() {
    System.out.println(
      "Current conditions are: \n" + 
      "humidity: " + this.humidity + "\n" + 
      "temperature:" + this.temperature + "\n"
      );
  }

  public void update(Observerable o, Object args) {
    if(o instanceof WeatherData) {
      this.temperature = o.getTemperature();
      this.humidity = o.getHumidity();
      this.display();
    }
  }
}
