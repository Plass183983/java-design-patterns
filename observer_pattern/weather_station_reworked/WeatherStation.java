package weather_station;

import weather_station.observers.HeatIndexDisplay;
import weather_station.observers.CurrentConditionsDisplay;
import weather_station.observers.StatisticsDisplay;

public class WeatherStation {
  public static void main(String[] args) {
    WeatherData weatherData = new WeatherData();
    CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(weatherData);
    StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
    HeatIndexDisplay heatIndexDisplay = new HeatIndexDisplay(weatherData);

    // First day
    weatherData.setMeasurements(25.0f, 23.3f, 5.3f);

    // Second day
    weatherData.setMeasurements(19.9f, 20.5f, 11.3f);

    // Third day
    weatherData.setMeasurements(55f, 27.1f, 21.3f);
  }
}
