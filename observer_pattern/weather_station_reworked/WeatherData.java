package weather_station_reworked;

import java.util.Observable;
import java.util.Observer;

public class WeatherData extends Observer {
  private float humidity;
  private float temperature;
  private float pressure;

  public WeatherData() {
  }

  public void measurementsChanged() {
    setChanged();
    notifyObservers();
  }

  public void setMeasurements(float humidity, float temperature, float pressure) {
    this.humidity = humidity;
    this.temperature = temperature;
    this.pressure = pressure;
    this.measurementsChanged();
  }

  public float getHumidity() {
    return this.humidity;
  }

  public float getTemperature() {
    return this.temperature;
  }

  public float getPressure() {
    return this.pressure;
  }
}
