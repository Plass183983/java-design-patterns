package starbuzz;

import starbuzz.beverages.*;
import starbuzz.decorators.*;

public class Main {
  public static void main(String[] args) {

    // Order for a simple espresso
    Beverage beverage = new Espresso();
    System.out.println(beverage.getDescription() + ": " + beverage.cost());

    System.out.println("##################################### \n");

    // House Blend with some extras
    // * Mocha and Whip are decorators * 
    Beverage houseBlend = new HouseBlend();
    houseBlend = new Mocha(houseBlend);
    houseBlend = new Mocha(houseBlend);
    houseBlend = new Whip(houseBlend);
    System.out.println(houseBlend.getDescription() + ": " + houseBlend.cost());
  }
}