package starbuzz.beverages;

public class HouseBlend extends Beverage {
  
  public HouseBlend() {
    this.price = 5.0;
    this.description = "House Blend";
  }

  public double cost() {
    return this.price;
  }
}
