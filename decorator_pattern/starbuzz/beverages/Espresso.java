package starbuzz.beverages;

public class Espresso extends Beverage {
  public Espresso() {
    this.price = 2.0;
    this.description = "Espresso";
  }

  public double cost() {
    return this.price;
  }
}
