package starbuzz.beverages;

public abstract class Beverage {
  protected String description = "Unknown Drink";
  protected double price = 0.0;

  public String getDescription() {
    return this.description;
  }

  public abstract double cost();
}