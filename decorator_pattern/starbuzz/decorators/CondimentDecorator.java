package starbuzz.decorators;

import starbuzz.beverages.Beverage;

public abstract class CondimentDecorator extends Beverage{
  protected Beverage beverage;

  public CondimentDecorator(Beverage beverage) {
    this.beverage = beverage;
  }

  public Beverage getBeverage() {
    return this.beverage;
  }

  public String getDescription() {
    return this.beverage.getDescription() + ", " + this.description;
  }
}