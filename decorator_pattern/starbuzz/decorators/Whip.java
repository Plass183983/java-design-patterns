package starbuzz.decorators;

import starbuzz.beverages.Beverage;

public class Whip extends CondimentDecorator {
  public Whip(Beverage beverage) {
    super(beverage);

    this.price = .15;
    this.description = "Whip";
  }

  public double cost() {
    return this.beverage.cost() + this.price;
  }
}
