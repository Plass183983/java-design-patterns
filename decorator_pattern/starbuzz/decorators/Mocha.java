package starbuzz.decorators;

import starbuzz.beverages.Beverage;

public class Mocha extends CondimentDecorator {
  
  public Mocha(Beverage beverage) {
    super(beverage);
    this.price = .25;
    this.description = "Mocha";
  }

  public double cost() {
    return this.price + this.beverage.cost();
  }
}
